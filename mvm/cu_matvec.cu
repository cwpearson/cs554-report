#include "cu_matvec.hpp"

#include <cuComplex.h>
#include <cublas_v2.h>
#include <iostream>
#include <cstdio>

#define CUDA_CHECK(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

void mvm_gpu_cublas(const std::complex<double> *m, 
                    const std::complex<double> *v, 
                    std::complex<double> *p, 
                    const size_t sizey,
                    const size_t sizex) {


  cuDoubleComplex *p_d, *v_d, *m_d;

  // Allocate device memory
  CUDA_CHECK( cudaMalloc(&p_d, sizey * sizeof(cuDoubleComplex)) );
  CUDA_CHECK( cudaMalloc(&v_d, sizex * sizeof(cuDoubleComplex)) );
  CUDA_CHECK( cudaMalloc(&m_d, sizey * sizex * sizeof(cuDoubleComplex)) );

  // Copy to device
  CUDA_CHECK( cudaMemcpy(v_d, v, sizex * sizeof(cuDoubleComplex), cudaMemcpyHostToDevice) );
  CUDA_CHECK( cudaMemcpy(m_d, m, sizey * sizex * sizeof(cuDoubleComplex), cudaMemcpyHostToDevice) );

  cudaMemset(p_d, 0, sizey * sizeof(cuDoubleComplex)); 

  cublasStatus_t status;
  cublasHandle_t handle;
  status = cublasCreate(&handle);
  if (status != CUBLAS_STATUS_SUCCESS) {
    std::cerr << "CUBLAS error\n";
    exit(EXIT_FAILURE);
  }

  const cuDoubleComplex ONE = make_cuDoubleComplex(1,0);

  status = cublasZgemv(handle, CUBLAS_OP_T, sizex, sizey, 
              &ONE,
              m_d, sizex,
              v_d, 1,
              &ONE, 
              p_d, 1);
  if (status != CUBLAS_STATUS_SUCCESS) {
    std::cerr << "CUBLAS failure\n";
    exit(EXIT_FAILURE);
  }


  // Copy to host
  CUDA_CHECK(cudaMemcpy(p, p_d, sizey * sizeof(cuDoubleComplex), cudaMemcpyDeviceToHost) );

  // Free device memory
  cudaFree(p_d); 
  cudaFree(v_d);
  cudaFree(m_d);
  //printf("(CUDA) Freed GPU memory.\n");

}

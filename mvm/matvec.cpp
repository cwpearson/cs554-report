#include <omp.h>
#include "matvec.hpp"

using namespace std;

void row_major(const complex<double> * __restrict__ m, 
               const complex<double> * __restrict__ v, 
               complex<double> * __restrict__ p, 
               const size_t sizey, const size_t sizex,
               const size_t num_threads){

  omp_set_num_threads(num_threads);

  #pragma omp parallel for
  for (size_t y = 0; y < sizey; ++y) {
    complex<double> acc(0,0);
    for (size_t x = 0; x < sizex; ++x) {
      acc += m[y*sizex+x]*v[x];
    }
    p[y] = acc;
  }
}

#include "matvec.hpp"
#include "cu_matvec.hpp"
#include "tictoc.hpp"

#include <vector>
#include <complex>
#include <iostream>

using namespace std;

void generate(vector<complex<double>> &m,
              vector<complex<double>> &v,
              vector<complex<double>> &p_ref,
              vector<complex<double>> &p_test,
              const size_t sizey, const size_t sizex) {

  m.resize(sizey * sizex);
  v.resize(sizex);
  p_ref.resize(sizey);
  p_test.resize(sizey);

  for (size_t y = 0; y < sizey; ++y) {
    for (size_t x = 0; x < sizex; ++x) {
      m[y * sizex + x] = complex<double>(rand(), rand());
    }
  }

  for (size_t x = 0; x < sizex; ++x) {
    v[x] = complex<double>(rand(), rand());
  }
}

int main(void) {

  const size_t sizey { 11304 };
  const size_t sizex { 11304 };

  const size_t flops = sizey * sizex * (2 + 7);
  const size_t mem = 16 * (sizey * sizex + sizey + sizex);

  vector<complex<double>> m;
  vector<complex<double>> v;
  vector<complex<double>> p_cpu, p_gpu;

  generate(m, v, p_cpu, p_gpu, sizey, sizex);

  std::cout << "num_threads\telapsed\tmem\tgflops\n";
  for (size_t num_threads = 0; num_threads < 24; num_threads += 1) {
    if (!num_threads) {
      num_threads = 1;
    }
    double elapsed = 0;
    tic();
    for (size_t i = 0; i < 2; ++i) {
      row_major(m.data(), v.data(), p_cpu.data(), sizey, sizex, num_threads);
    }
    elapsed += toc();
    elapsed /= 2;
    std::cout << num_threads << "\t" 
              << elapsed << "\t"
              << double(mem) / elapsed / 1e9 << "\t"
              << double(flops) / elapsed / 1e9 << "\n";
  }

}

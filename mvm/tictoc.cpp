#include "tictoc.hpp"
#include <stack>
#include <cstdlib>
#include "sys/time.h"

using namespace std;

stack<double> timerStack;

void tic(void) {
  timeval start;
  gettimeofday(&start, NULL);
  timerStack.push(start.tv_sec + start.tv_usec / 1e6);
}


double toc(void) {
  timeval stop;
  gettimeofday(&stop, NULL);
  double start = timerStack.top();
  timerStack.pop();
  return (stop.tv_sec + stop.tv_usec / 1e6) - start;

}

#ifndef TICTOC_HPP
#define TICTOC_HPP

// Start the timer
void tic(void);

// stop the timer
double toc(void);

#endif

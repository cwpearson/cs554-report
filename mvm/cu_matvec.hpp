#ifndef CU_MATVEC_HPP
#define CU_MATVEC_HPP

#include <complex>

void mvm_gpu_cublas(const std::complex<double> *m, 
                    const std::complex<double> *v, 
                    std::complex<double> *p, 
                    const size_t sizey,
                    const size_t sizex);

#endif

#ifndef MATVEC_HPP
#define MATVEC_HPP

#include <complex>

void row_major(const std::complex<double> *m, 
               const std::complex<double> *v, 
               std::complex<double> *p, 
               const size_t sizey, const size_t sizex,
               const size_t num_threads);

#endif

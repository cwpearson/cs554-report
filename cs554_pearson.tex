\documentclass[11pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{listings}
\usepackage{color}
\usepackage{paralist}
\usepackage{graphicx}
\usepackage[font=small]{caption}
\usepackage{subcaption}
\usepackage{hyperref}
\usepackage[]{algorithm2e}
\usepackage{pdfpages}

\lstset{
    language=C++,
    basicstyle=\tiny,
    keywordstyle=\color{blue}\tiny,
    commentstyle=\color{magenta}\tiny,
    stringstyle=\color{red}\tiny
}

\begin{document}

\setlength{\baselineskip}{14pt}

\title{CS554 Final Project Report \\
       GPU Acceleration of the Biconjugate Gradient Stabilized Method}
\author{Carl Pearson \\
        \texttt{pearson@illinois.edu}}
\date{December 8, 2015}

\maketitle

\section{Introduction}

Forward solvers are a class of computational electromagnetics applications
that compute a scattered EM field when given an arbitrary geometry and set of
field sources - for example, a forward solver may be used to determine how an
airplane scatters a radar signal. Forward solvers are also the core component
in iterative \textit{backward} solvers, which derive an object geometry when
given a set of sources and a scattered field.

Backward solvers use a series of sequential forward solutions: the field
source is applied to a test geometry to produce a scattered field, and then
based on the different between the computed and known scattered field, the
test geometry is refined to more closely match the true geometry. To solve the
backwards problem quickly the forward problem must also be solved quickly.

The forward field solver has 3 components:
\begin{enumerate}
  \item{Compute geometric interaction matrix.}
  \item{\textbf{Solve linear system (subject of this report).}}
  \item{Compute scattered field.}
\end{enumerate}

\textbf{The geometry is characterized by dielectric values} that
are geometrically discretized to one-tenth of the wavelength ($\lambda / 10$).
Each of
these dielectric values is a \textbf{complex number} - the real part corresponds
to the
permittivity, and the imaginary part corresponds to absorption or loss.

An \textit{interaction matrix} $\mathbf{A}$ is constructed, where element $i,j$
of the matrix
encodes how the field from element $i$ affects element $j$, and vis-versa. This
matrix is
symmetric for homogeneous geometries, but it is not symmetric in general. It
follows that $\mathbf{A}$ is a dense $n \times n$ matrix, where $n$ is the
number of discretized geometry elements. A vector $\mathbf{b}$ of size $n$
that corresponds to the incident field is also constructed during this phase.
\textbf{The core component of the field solver solves the dense linear system}
$\mathbf{A}\mathbf{x}=\mathbf{b}$. $\mathbf{x}$ is the unknown field
coefficients that must be determined.

Although they are traditionally associated with sparse systems,
\textbf{iterative solvers are used in this context due to performance that is
better-than the $O(n^3)$} provided by direct solvers. Thought each iteration is
$O(n^2)$, the problem is well-conditioned and the number of iterations grows
much more slowly than $n$, and for practical purposes can be considered to have
a finite upper limit regardless of geometric properties that influence
$\mathbf{A}$.

\section{Algorithm Description}
\label{sec:alg}
Algorithm \ref{alg:bicgs} shows the Biconjugate gradient stabilized method for
solving a linear system.

\RestyleAlgo{boxruled}
\LinesNumbered
\begin{algorithm}[H]
 \caption{Preconditioned Biconjugate Gradient Stabilized Method.}
 \label{alg:bicgs}
 \KwData{input matrix $\mathbf{A}$, input vector $\mathbf{b}$}
 \KwResult{ Solution $\mathbf{x}$ to system $\mathbf{Ax} = \mathbf{b}$}
 $\mathbf{x}_0 = \mathbf{v}_0 = \mathbf{p}_0 = \mathbf{0}$\;
 $\mathbf{r}_0 = \mathbf{b} - \mathbf{Ax}$\;
 $\rho_0 = \alpha = \omega_0 = 1$\;
 \For{$i = 0,1,2,...$}{
  $\rho_i = (\mathbf{\hat{r}}_0, \mathbf{\hat{r}}_{i-1})$\;
  $\beta_i = (\rho_i / \rho_{i-1})(\alpha / \omega_{i-1})$\;
  $\mathbf{p}_i = (\mathbf{r}_{i-1} + \beta(\mathbf{p}_{i-1}-\omega_{i-1}\mathbf{v}_{i-1})$\;
  $\mathbf{\hat{p}} = \mathbf{K}^{-1}\mathbf{p}_i$\;
  $\mathbf{v} = \mathbf{A}\mathbf{\hat{p}}$\;
  $\alpha = \rho_i/(\mathbf{\hat{r}}_0, \mathbf{v}_i)$\;
  $\mathbf{s} = \mathbf{r}_{i-1} - \alpha\mathbf{v}_i$\;
  \If{$||s||$ is small}{
    $\mathbf{x}_i = \mathbf{x}_{i-1}+\alpha\mathbf{\hat{p}}$\;
    quit\;
  }
  $\mathbf{\hat{s}} = \mathbf{K}^{-1}\mathbf{s}$\;
  $\mathbf{t} = \mathbf{A}\mathbf{\hat{s}}$\;
  $\omega_i=(\mathbf{K}_1^{-1}\mathbf{t}, \mathbf{K}_1^{-1}\mathbf{s})/
            (\mathbf{K}_1^{-1}\mathbf{t}, \mathbf{K}_1^{-1}\mathbf{t})$\;
  $\mathbf{x}_i = \mathbf{x}_{i-1}+\alpha\mathbf{\hat{p}}+\omega_i\mathbf{z}$\;
  \If{$\mathbf{x}_i$ is accurate enough}{
    quit\;
  }
  $\mathbf{r}_i = \mathbf{s}-\omega_i\mathbf{t}$
 }
\end{algorithm}

Table \ref{tab:bigo} describes the asymptotic running time for
each step of the algorithm. As exemplified in the following sections, the
running time is dominated by the matrix-vector multiplication.
\textbf{Accelerating the matrix-vector multiplication is therefore the
 focus of this project.}

\begin{table}[h!]
  \begin{center}
    \begin{tabular}{|c|c|c|}
      \hline
      \textbf{Step} & \textbf{Description} & \textbf{Asymptotic Time} \\ \hline
      2  & matrix-vector multiplication & $O(n^2)$ \\ \hline
      5-7 & generate refinement & $O(n)$ \\ \hline
      8  & Jacobi preconditioner & $O(n)$ \\ \hline
      9  & \textbf{matrix-vector multiplication} & $O(n^2)$ \\ \hline
      10-12 & check convergence and apply refinement & $O(n)$ \\ \hline
      16 & Jacobi preconditioner & $O(n)$ \\ \hline
      17 & \textbf{matrix-vector multiplication} & $O(n^2)$ \\ \hline
      18-20, 23 & Generate refinement and check convergence & $O(n)$ \\ \hline
    \end{tabular}
  \end{center}
  \caption{Asymptotic sequential running times of selected BiCGSTAB steps
  (refer to
  Algorithm \ref{alg:bicgs}). Asymptotic and actual running time is dominated
  by matrix-vector multiplications (Section \ref{sec:baselineperf}).}
  \label{tab:bigo}
\end{table}

The first matrix-vector multiplication in each BiCGSTAB iteration is
$\mathbf{v} = \mathbf{A}\mathbf{\hat{p}}$
and the second is
$\mathbf{t} = \mathbf{A}\mathbf{\hat{s}}$.

\subsection{BiCGSTAB Performance Model}

The total execution time can be modeled as

$$ T_{bicgs,cpu} = (2I+1)(T_{mvm,cpu}) + I(T_{other}) $$

Where $T_{mvm}$ is the time taken to do a double-precision complex dense
matrix-vector multiplication, $T_{other}$ is the time taken to do the rest of
the $O(n)$ operations, and $I$ is the number of iterations until convergence
(modeled as a fixed finite number). There are two matrix-vector multiplications
per iteration, plus one at the beginning to get the initial residual from the
initial guess.

\section{Performance Baseline}

The first step was to develop a C++ baseline implementation. Due to the
characteristics of the BiCGSTAB algorithm,
the C++ implementation can make good use of multi-core chips through OpenMP.

\subsection{Test System}
The test system is a \textbf{single node} with a 4-core CPU that supports 8
hardware
threads through SMT. The GPU is a NVIDIA Kepler GPU with 2880 CUDA cores and
12GB of GDDR5 RAM.

% FIXME: add memory bandwidth?
\begin{table}[h!]
  \begin{center}
    \begin{tabular}{|c|l|}
      \hline
      \textbf{Component} & \textbf{Description} \\ \hline
      CPU & ​Intel Xeon E5520 (4C8T) \\ \hline
      GPU & NVIDIA TESLA K40c \\ \hline
      RAM & 24GB DDR2-1333 \\ \hline
      BUS & PCIe 2.0 x16 \\ \hline
    \end{tabular}
  \end{center}
  \caption{Test system configuration.}
  \label{tab:testsys}
\end{table}

\subsection{Matrix-Vector Multiplication C++ / OpenMP Implementation and
            Performance Model}
\label{sec:baselineperf}

The baseline BiCGSTAB implementation is written in C++. Each step of
BiCGSTAB is essentially a vector reduction, a BLAS1, or BLAS2 operation.
It is relatively
straightforward to create an well-performing implementation of
any of those operations for large inputs because the operations are
fundamentally memory-bandwidth-limited on current CPU architectures
and because is little-to-no data reuse in any of the operations.

As discussed in the course, the serial performance model for matrix-vector
multiplication is

$$ T_1 = n^22t_c $$.

This reflects the dot product over $n$ values needed for each of $n$ resulting
vector entries. Consider a more detailed model, where $t_c$ is the time for a
floating-point operation, $t_m$ is the time for a memory read or write. Under
the assumption that the input vector is cached and therefore only read once,
the model becomes

$$ T_1 = n^2(2t_c + t_m) + 2nt_m $$

We will consider $n > 1000$, so the lower-order term may be ignored.

$$ T_1 = n^2(2t_c + t_m) + 2nt_m $$

A more reasonable baseline is a simple shared-memory parallel CPU
implementation with $p$ processes. In this implementation, the output vector
and input rows are
partitioned among the threads, leading to an embarrassingly-parallel
implementation that needs no synchronization or communication. The input vector
is read once for each input row. Figure \ref{fig:simplerow} shows this
graphically.

\begin{figure}[!ht]
  \centering
    \includegraphics[width=0.7\textwidth]{images/simple_row.pdf}
  \caption { Mapping of threads to data in $\mathbf{Ax} = \mathbf{b}$. Each
  thread \texttt{p} computes the dot product of multiple rows of $\mathbf{A}$
  and $mathbf{x}$ to produce multiple elements of $\mathbf{b}$.
    $\mathbf{x}$ and $\mathbf{b}$ are stored sequentially, and $\mathbf{A}$ is
    stored in row-major order.
  }
    \label{fig:simplerow}
\end{figure}

 It might then
seem natural to model the multi-threaded performance as

$$ T_p = T_1 / p $$

This assumes that the maximum aggregate performance is $p$ times the maximum
thread performance. In practice this is not true for memory-bandwidth-bound
computations like matrix-vector multiplication. A more accurate model has
$t_m$ vary with $p$ in the following way: let the memory rate $ r_m = 1/t_m $.
Also assume there is some maximum
aggregate memory bandwidth rate $r_{max}$ for the entire chip. Then, the rate
at which $p$ processes can move memory is

$$ r_m = \min(pr_m, r_{max})$$

giving a full multiplication performance model of

$$ T_p = (n^2)(\frac{2t_c}{p} + \frac{1}{r_m}) =
         (n^2)(\frac{2t_c}{p} + \frac{1}{\min(pr_m, r_{max})})$$

In short, for ``small'' $p$ there is a roughly linear increase in achievable
memory bandwidth, and for larger $p$ each thread gets a proportionally-smaller
slice of $r_{max}$. Figure \ref{fig:mvmrmax} shows this phenomenon empirically
on the test system.

\begin{figure}[ht]
  \centering
    \includegraphics[width=0.7\textwidth]{images/mvm_rmax.png}
  \caption {
    Plot of empirically-measured achieved memory bandwidth during dense
    matrix-vector multiplication.
    For small $p$, memory bandwidth is approximately proportional to $p$ (the
    sloped line). For
    larger $p$, memory bandwidth is constant (the horizontal line, $r_{max}$).
  }
    \label{fig:mvmrmax}
\end{figure}

Figures \ref{fig:mvmruntime} and \ref{fig:mvmefficiency} show the run time and
the parallel efficiency, respectively.

\begin{figure}[ht]
  \centering
    \includegraphics[width=0.7\textwidth]{images/mvm_runtime.png}
  \caption {
  Plot of empirically-measured and model matrix-vector multiplication time
  with parameters $t_c=10^{-12}$, $n=11304$, $r_m=4\times 10^{8}$, $r_{max} = 1*10^9$.
  }
    \label{fig:mvmruntime}
\end{figure}

\begin{figure}[ht]
  \centering
    \includegraphics[width=0.7\textwidth]{images/mvm_efficiency.png}
  \caption { Plot of empirically-measured and model efficiency with parameters
  $t_c=10^{-12}$, $n=11304$, $r_m=4\times 10^{8}$, $r_{max} = 1*10^9$.
  }
    \label{fig:mvmefficiency}
\end{figure}

The efficiency of this implementation is independent of the matrix size for
matrices large enough to be relevant to this problem. A more detailed
performance model utilizing knowledge of the OpenMP runtime would be required
if smaller matrices were considered.

$$ E = \frac{T_1}{pT_p} =
       \frac{n^2t_c}{(n^2)(\frac{2t_c}{p} + \frac{1}{\min(pr_m, r_{max})})} =
       \frac{t_c}{(\frac{2t_c}{p} + \frac{1}{\min(pr_m, r_{max})})}  $$

This leads to trivial isoefficiency curves, so they are omitted.

\textbf{This suggests $r_m \approx 5\mathrm{GB/s}$, with
a peak achievable rate of 16 GB/s. Even if the program were completely
optimized, the theoretical bandwidth limit of 25.6 GB on this processor limits
the maximum performance to less than twice as fast as this simple
code.} That rate may not even be achievable
with dense matrix-vector multiplication, which is heavily skewed towards reads.
\textbf{Since only a small percentage of the data is reused (the input vector), there
is no opportunity to provide significant speedup through a cache-aware
application.} Determination of $t_c$ is ignored since it is apparent that the
application is severely memory-bound. So, the overall performance model becomes

$$ T_{bicgs,cpu} = (2I+1)( T_{mvm,cpu} ) + I( T_{other} ) $$

where

$$ T_{mvm,cpu} = \frac{n^2}{p}(2t_c + \frac{1}{\min(pr_m, r_{max})})$$

\begin{table}[h!]
  \begin{center}
    \begin{tabular}{|c|c|c|c|}
      \hline
      \textbf{Geometry Elements} ($n$)    & \textbf{2828}&\textbf{5021}&\textbf{11304} \\ \hline
      \textbf{Total BiCGSTAB Time (s)}    & 7.686 & 37.14 & 110.513 \\ \hline
      \textbf{BiCGSTAB Iterations}        & 326   & 421   & 303​ \\ \hline
      \textbf{Average Iteration Time (s)} & 0.023 & 0.087 & 0.361 \\ \hline
      \textbf{Average MVM Time (s)}       & 0.011 & 0.043 & 0.181 \\ \hline
      \textbf{MVM Time Contribution (\%)} & 96.25 & 98.35 & 99.14 \\ \hline
    \end{tabular}
  \end{center}
  \caption{Performance results for multi-threaded CPU implementation. As $n$
  grows, the percentage of time contributed by matrix-vector multiplication
  becomes completely dominant. The number of BiCGSTAB iterations can vary
  depending on specific values of $\mathbf{A}$, so average times per iteration
  and matrix-vector multiplication are also provided. It can be observed that
  MVM and total BiCGSTAB time grow with $n^2$.}
  \label{tab:cpuperf}
\end{table}

The only way around this performance limitation under the current algorithm is
to provide more memory bandwidth. This can be done by distributing the work
across multiple nodes (using their memory busses in parallel), and/or
\textbf{offloading the work to an accelerator that has much higher memory
bandwidth.} This report takes the second approach, since the target system has
accelerators available and node parallelism will be used for a different part of
the overall application.

\section{CUDA Implementation}

The target accelerator, an NVIDIA Tesla K40c, has 288 GB/s of global memory
bandwidth. Utilized properly, that should allow for a $>$10-fold
improvement over the CPU implementation. NVIDIA's GPU programming environment
and kernel language is CUDA.

\subsection{GPU Programming Model}
\label{sec:gpuprogramming}
The GPU programming environment consists of a \textit{host} (the CPU and
its associated DRAM) and the \textit{device} (the GPU). A sequential thread
running on the host launches a CUDA \textit{kernel} function that executes on
the device. The kernel is divided into threads that are addressed numerically
through a two-level hierarchy: a grid of thread blocks, each of which consists
of multiple threads. Any particular thread in a kernel will make control and
data-access decisions based on its thread and thread-block IDs.

Thread-blocks are assigned to execution units in a round-robin fashion. Groups
of 32
threads within a thread block are called \textit{warps} and execute on those
units
in a lockstep SIMD fashion. If threads in a warp take different control-flow
paths, the entire warp executes all paths and masks off inactive threads at the
appropriate time. The actual execution is in-order. When a warp becomes stalled
due to a long-latency access, another warp resumes execution in its place at
single-cycle granularity. This effectively hides memory-access latency even
with in-order execution and small caches.

The host is also responsible for providing data for the device to operate on.
This is accomplished through two types of API calls: the first type allocates or
frees space on the device DRAM, and the second type copies data between the
host and device DRAM over the PCIe bus.

Table \ref{tab:cudasteps} shows a very abstract version of a C/C++ program, and
the additional steps added when CUDA is used. Even if the GPU compute kernel is
faster than the corresponding CPU code, there is some overhead in steps 2a, 2b,
and 3a that were not present in the original CPU code. As this report shows in
Section \ref{sec:removetransfer}, minimizing that overhead is crucial for good
accelerated performance.

\begin{table}[h!]
  \begin{center}
    \begin{tabular}{|c|c|c|}
      \hline
      \textbf{Step} & \textbf{C/C++ Only} & \textbf{CUDA-Specific} \\ \hline
      1 & allocate host memory & -- \\ \hline
      2 & initialize host memory & -- \\ \hline
      2a & -- & allocate device memory \\ \hline
      2b & --  & memcpy host $\rightarrow$ device \\ \hline
      3 & do computation & doc computation \\ \hline
      3a & -- & memcpy host $\leftarrow$ device \\ \hline
    \end{tabular}
  \end{center}
  \caption{Abstracted C/C++ program, and steps added when using CUDA. Steps 2a
           and 2b provide the GPU with the data it will operate on. Step 3
           replaces the CPU computation with the same kernel on the GPU. Step
           3a copies the resulting data back to the CPU.}
  \label{tab:cudasteps}
\end{table}

\subsection{Speedup Estimate with GPU Data Allocation and Transfer}

The right-hand column of table \ref{tab:cudasteps} lists the additional program
steps that are added to
allocate space in the GPU DRAM and transfer data between the GPU and the host.
This suggests the first GPU performance model:

$$ T_{bicgs,gpu} = (2I+1)(T_{n\times n + 2n, alloc} +
                          T_{n\times n + n, h\rightarrow d} +
                          T_{mvm,gpu} + T_{n, h\leftarrow d}) +
                   I( T_{other}) $$

where $T_{n\times n + 2n, alloc}$ is the time it takes to allocate one
$n\times n$ matrix and two $n$-vectors in the GPU DRAM,
$T_{n\times n + n, h\rightarrow d}$ is the time it takes to move an
$n\times n$ matrix and $n$-vector from the host to device (the input matrix and
vector), and $T_{n, h\leftarrow d}$ is the time it takes to move an $n$-vector
(the result vector) from the device to host.
For this to be a net performance improvement,

$$ T_{n\times n + 2n, alloc} +
   T_{(n\times n + n), h\rightarrow d} +
   T_{mvm,gpu} +
   T_{n, h\leftarrow d} < T_{cpu,mvm} $$

A back-of-the-envelope estimate can be used to determine determine how
profitable this GPU implementation will be. Table \ref{tab:estimate} shows
the estimated values for each expression in the inequality.

%FIXME: caption
\begin{table}[!ht]
  \begin{center}
    \begin{tabular}{|c|c|c|}
      \hline
      \textbf{Step} & \textbf{Elapsed Time} & \textbf{Description} \\ \hline
      $T_{n\times n + 2n, alloc}$ & $\approx 0$ & Much less time than any other step \\ \hline
      $T_{(n\times n + n), h\rightarrow d}$ & $\frac{n^2}{8\mathrm{GB/s}}$ & memcpy over PCIe at 8GB/s \\ \hline
      $T_{mvm,gpu}$ & $\frac{n^2}{288\mathrm{GB/s}}$ & memory-bandwidth limited at 288 GB/s \\ \hline
      $T_{n, h\leftarrow d}$ & $\frac{n}{8\mathrm{GB/s}}$ & memcpy over PCIe at 8GB/s \\ \hline
      $T_{mvm,cpu}$ 3 & $\frac{n^2}{16\mathrm{GB/s}}$ & Achieved memory bandwidth of 16 GB/s \\ \hline
    \end{tabular}
  \end{center}
  \caption{Estimate of performance for GPU performance model expressions. All
  steps except $T_{mvm,cpu}$ are based off of maximum bandwidth values.
  $T_{mvm,cpu}$ is based off of the empirical values developed in Section
  \ref{sec:baselineperf}.}
  \label{tab:estimate}
\end{table}

The inequality then becomes

$$ 0 +
   \frac{n^2}{8\mathrm{GB/s}} +
   \frac{n^2}{288\mathrm{GB/s}} +
   \frac{n}{8\mathrm{GB/s}} <
   \frac{n^2}{16\mathrm{GB/s}} $$

dropping the lower-order terms yields

$$ \frac{n^2}{8\mathrm{GB/s}} +
   \frac{n^2}{288\mathrm{GB/s}} <
   \frac{n^2}{16\mathrm{GB/s}} $$

which is obviously false. \textbf{Even if maximum
memory bandwidth on the GPU and in the data transfer can be achieved, the
matrix-vector multiplication under this approach will have a speedup of}
$\approx 1/2$.

\subsection{Performance Model: Elision of GPU Data Allocation and Transfer}
\label{sec:removetransfer}
Fortunately, the matrix $\mathbf{A}$ is static throughout BiCGSTAB. This means
that it does not need to be copied to the GPU in every iteration (as long as
the entire matrix fits in the GPU DRAM). Furthermore, memory does not need to
be allocated or freed in each iteration since the size of the vectors and
matrices is static throughout. This yields a revised performance
model:

$$ T_{bicgs,gpu} = T_{n\times n + 2n, alloc} + T_{n\times n, h\rightarrow d} +
              (2I+1)(T_{n, h\rightarrow d} + T_{mvm,gpu} + T_{n, h\leftarrow d}) +
               I( T_{other}) $$

The small transfers $T_{n, h\rightarrow d}$ and $T_{n, h\leftarrow d}$
correspond to moving $\mathbf{v}$, $\mathbf{\hat{p}}$, $\mathbf{\hat{s}}$,
 and $\mathbf{t}$ (refer to Algorithm \ref{alg:bicgs})
from the host to device or vis-versa. Now, speedup within the BiCGS iteration
loop is theoretically achievable as long as $n$ is sufficiently large:

$$ T_{n, h\rightarrow d} + T_{gpu,mvm} + T_{n, h\leftarrow d} < T_{cpu,mvm} $$

or

$$ \frac{n}{8\mathrm{GB/s}} +
   \frac{n^2}{288\mathrm{GB/s}} +
   \frac{n}{8\mathrm{GB/s}} <
   \frac{n^2}{16\mathrm{GB/s}} $$


\subsection{Striving for the GPU Memory Bandwidth Limit: Row-Major Matrix-Vector
            Multiplication}

Similarly to the multi-threaded CPU implementation described in Section
\ref{sec:baselineperf}, GPU threads can be mapped to each row, making each
thread responsible for a full dot-product and element of the output vector.
Figure \ref{fig:gpurow} shows the mapping of threads to data in the naive
row-major matrix-vector multiplication kernel. Thread blocks (\texttt{tb}) are
mapped across rows, and each thread (\texttt{th}) within a thread block is
responsible for an adjacent row of the matrix.

% FIXME: caption
\begin{figure}[!ht]
  \centering
    \includegraphics[width=1\textwidth]{images/gpu_row.pdf}
  \caption { Mapping of threads to data in the row-major matrix-vector
  multiplication GPU kernel. Thread blocks (\texttt{tb}) are assigned to
  consecutive chunks of rows of $\mathbf{A}$, and threads ()\texttt{th}) within
  the thread blocks are assigned to consecutive rows within the chunks.
  All threads share reads from $\mathbf{x}$ and write to independent elements of
  $\mathbf{b}$. $\mathbf{A}$ is stored in column-major order.
    $\mathbf{x}$ and $\mathbf{b}$ are stored sequentially.
  }
    \label{fig:gpurow}
\end{figure}

Achieved DRAM bandwidth results for row-major dense matrix-vector
multiplication are shown in table \ref{tab:rowmajor}. Since the matrix
dimensions are so large, the amount of data loaded is assumed to be 16 bytes
per matrix entry, and the useful DRAM bandwidth achieved can be computed by
$ \frac{\mathrm{data\, used}}{\mathrm{kernel\, time}} $.

\begin{table}[h!]
  \begin{center}
    \begin{tabular}{|c|c|c|}
      \hline
      \textbf{Matrix Size} & \textbf{Row-Major Kernel Time (s)} & \textbf{Useful DRAM Bandwidth (GB/s)} \\ \hline
      $2828 \times 2828$   & 0.00271 & 43.975 \\ \hline
      $5021 \times 5021$   & 0.00814 & 46.15 \\ \hline
      $11304 \times 11304$ & 0.06490 & 29.34 \\ \hline
    \end{tabular}
  \end{center}
  \caption{Kernel time (without memory transfer) and useful DRAM bandwidth
  for the row-major matrix-vector multiplication kernel. Useful DRAM bandwidth
  is defined as $ \frac{\mathrm{data\, used}}{\mathrm{kernel\, time}} $, where
  $\mathrm{data\, used}$ is $16 \times$ the matrix size, since each matrix
  element is
  a double-precision complex value.  }
  \label{tab:rowmajor}
\end{table}

These DRAM bandwidth values are far below the theoretical 288 GB/s, of which
164 GB/s should be achievable in a read-dominated workload. A simple
data-layout transformation will capture the rest of the theoretically-possible
bandwidth, so further discussion of the row-major kernel is omitted.

\subsection{Column-Major Matrix-Vector Multiplication Performance Model}

In the row-major kernel shown in Figure \ref{fig:gpurow}, adjacent threads
(e.g. $\mathrm{th}_{0,0}$ and
$\mathrm{th}_{0,1}$) are accessing matrix elements that are $n \times 16$
bytes apart. In the column-major format, adjacent threads are accessing adjacent
elements.

NVIDIA GPU architectures feature \textbf{\textit{memory coalescing}}. Groups of
adjacent threads within a thread blocks have their memory accesses coalesced
into as few 512-byte DRAM accesses as possible. In the row-major scheme, since
adjacent threads access are separated by more than 512 bytes, there will be no
memory coalescing. In the column-major version, each group of 32 adjacent threads
will have its 16-byte element accesses coalesced into a single DRAM access, for
a \textbf{32-fold reduction in DRAM accesses}.

\begin{table}[h!]
  \begin{center}
    \begin{tabular}{|c|c|c|}
      \hline
      \textbf{Matrix Size} & \textbf{Column-Major Kernel Time (s)} & \textbf{Useful DRAM Bandwidth (GB/s)} \\ \hline
      $2828 \times 2828$   & 0.00182 & 65.5 \\ \hline
      $5021 \times 5021$   & 0.0034  & 110.45 \\ \hline
      $11304 \times 11304$ & 0.01111 & 171.38 \\ \hline
    \end{tabular}
  \end{center}
  \caption{Kernel time (without memory transfer) and useful DRAM bandwidth
  for the column-major matrix-vector multiplication kernel. Useful DRAM bandwidth
  is defined as $ \frac{\mathrm{data\, used}}{\mathrm{kernel\, time}} $, where
  $\mathrm{data\, used}$ is $16 \times$ the matrix size, since each matrix
  element is
  a double-precision complex value.  }
  \label{tab:colmajor}
\end{table}

For large $n$, this \textbf{kernel uses all of the theoretical DRAM read
bandwidth available in the GPU} ($\approx 1/2$ of 288 GB/s). For smaller values of $n$, the
thread-to-data mapping shown in Figure \ref{fig:gpucol} does not provide
enough parallelism to saturate GPU resources, so not enough DRAM accesses are
issued to fill the entire bandwidth.

\begin{figure}[!ht]
  \centering
    \includegraphics[width=1\textwidth]{images/gpu_col.pdf}
  \caption { Mapping of threads to data for column-major dense matrix-vector
    column-major multiplication kernel. \textbf{Note that the storage order}
    has changed for $\mathbf{A}$, and is now column-major.
    $\mathbf{x}$ and $\mathbf{b}$ are unchanged.
  }
    \label{fig:gpucol}
\end{figure}

The cost of this approach is that the matrix \textbf{$\mathbf{A}$ must be
transposed to
column-major order after it is generated.} While it is possible to do this when
$\mathbf{A}$ is generated, that is not currently done. This adds a new term to
the performance model.

$$ T_{bicgs,gpu} = T_{n\times n + 2n, alloc} + T_{n\times n, h\rightarrow d} +
                   T_{n\times n, transpose} +
              (2I+1)(T_{n, h\rightarrow d} + T_{mvm,gpu} + T_{n, h\leftarrow d}) +
               I( T_{other}) $$

NIVIDA's cuBLAS library is used for the transposition. Like dense matrix-vector
multiplication, \textbf{transposition is memory bound}, and therefore takes
approximately the same amount of time as a matrix-vector multiplication for a
same-sized matrix.

$$ T_{n\times n, transpose} \approx T_{mvm,gpu} $$

Given that hundreds of matrix-vector multiplies happen in each invocation of
BiCGSTAB, the transposition only contributes a fraction of a percent of the
final running time.

The host-to-device and device-to-host memory transfer rates are experimentally
determined to be $\approx$ 5.6GB/s for $n = 11304$. At that rate, the data
transfer in each direction takes approximately $30\mathrm{\mu s}$.

Table \ref{tab:speedup} shows the final performance results for BiCGSTAB over
a range of matrix sizes. Improving the performance of the matrix-vector
multiply has reduced it from 99\% to 85\% of the BiCGSTAB running time for
large matrices.

\begin{table}[h!]
  \begin{center}
    \begin{tabular}{|c|c|c|c|}
      \hline
      \textbf{Geometry Elements} ($n$)    & \textbf{2828}  & \textbf{5021}  & \textbf{11304} \\ \hline
      \textbf{BiCGSTAB Speedup}           & 4.88  & 10.6 & 13.98 \\ \hline
      \textbf{BiCGSTAB Iterations}        & 325   & 426   & 319​ \\ \hline
      \textbf{Average BiCGSTAB Iteration Speedup}  & 6.44 & 12.79 & 17.1 \\ \hline
      \textbf{Average MVM Speedup}        & 6.36  & 13.03 & 17.07 \\ \hline
      \textbf{MVM Time Contribution (\%)} & 78.52 & 80.51 & 84.39 \\ \hline
    \end{tabular}
  \end{center}
  \caption{BiCGSTAB and component speedup over the multi-threaded CPU
           implementation. The number of iterations is different than Table
           \ref{tab:cpuperf} due to different floating-point optimizations
           made by the CPU and GPU compilers. The MVM speedup does not include
           data transfer, which is on the order of 1/10 of 1\% of the kernel
           time.}
  \label{tab:speedup}
\end{table}

Figure \ref{fig:nvvp} shows a visualization generated by the NVIDIA Visual
Profiler. In it, even with the matrix-vector multiplication happening on the
GPU (\textbf{2} in the figure) there is now a discernible amount of computation
happening on the CPU. This corresponds to the 85\% of the time now taken in
matrix-vector multiplications on the GPU.

\begin{figure}[!ht]
  \centering
    \includegraphics[width=1\textwidth]{images/nvvp.pdf}
  \caption {Segment of output produced by the NVIDIA Visual Profiler.
  \textbf{1} annotates the start of the transposition. \textbf{2} annotates
  the start of the BiCGSTAB iterations. The long bar to the left of \textbf{2}
  is the initial calculation of the residual before the BiCGSTAB iterations
  start. \textbf{3} shows the small amount of time on the CPU used for the
  L2-norm and Jacobi Preconditioner before the first matrix-vector
  multiplication. \textbf{4} annotates the start of the next BiCGSTAB iteration.
  }
    \label{fig:nvvp}
\end{figure}

The final GPU performance model is

$$ T_{bicgs,gpu} = T_{n\times n + 2n, alloc} + T_{n\times n, h\rightarrow d} +
                   T_{n\times n, transpose} +
              (2I+1)(T_{n, h\rightarrow d} + T_{mvm,gpu} + T_{n, h\leftarrow d}) +
               I( T_{other}) $$

For reference, the CPU performance model was

$$ T_{bicgs,cpu} = (2I+1)(T_{mvm,cpu}) + I(T_{other}) $$

The GPU architectural features described in Section \ref{sec:gpuprogramming}
make a straightforward comparison of parallel efficiency or isoefficiency across
architectures problematic at best.\footnote{Even though it is a simple case,
the efficiency and isoefficiency discussion
in section \ref{sec:baselineperf} hopefully serve to demonstrate how such an
analysis is done.} The GPU processing characteristics are so different than the
CPU that it is impossible to satisfactorily answer the question of how many
``processors'' are used. Threads, warps, thread-blocks, or actual
hardware processors on the GPU could be used, but even so, the predicated
branch execution, in-order execution, small caches, and SIMD-style warp
execution drastically changes the execution characteristics of even simple code.


\section{Opportunities for Further Performance Improvement}
\begin{itemize}
\item{\textbf{Moving BiCGSTAB Loop to GPU:}By fusing some of the $O(n)$ BiCGSTAB
operations with the matrix-vector
multiplications on the GPU, the entire BiCGSTAB iteration loop could be executed
on the GPU. In the context of GPU computing,  those operations only provide very
limited parallelism (thousands of threads at most). The CPU time plus data
transfer is likely faster than the corresponding operations on the GPU.}
\item{\textbf{Pinned Memory:} When data is copied from the host to the device,
it is first copied to a \textit{pinned} memory buffer, from which the GPU DMA
engine can access it. Using this pinned memory buffer directly from the CPU code
removes this additional copy, which improves the achieved host-to-device and
device-to-host memory copy bandwidth.}
\item{\textbf{Removing Explicit Transpose:} In stead of introducing an explicit
data transpose, $\mathbf{A}$ could be stored in column-major form when it is
generated. Each element of $\mathbf{A}$ is computed from a compute-intensive
integral equation, so this would not significantly impact the performance of
storing $\mathbf{A}$.}
\item{\textbf{Coarse-Grained Geometry for better Initial Guess $x_0$}:
In order to reduce the number of iterations, it may be possible to solve the
system corresponding to a more coarse-grained discretization of the geometry,
then interpolate that solution as the first guess of the fine-grained
geometry.}
\end{itemize}

\section{Conclusion}

\textbf{
Through a relatively simple transformation of parallel CPU to GPU code, the time
to solve a large  well-conditioned linear system using BiCGSTAB can be reduced
by about 17x, from 110s to 7.9s. The relatively simple algorithm allows for a
transparent performance model, and the memory-bandwidth-limited nature of
the matrix-vector multiplication can be exploited for straightforward analysis
of tradeoffs. It also provides the ability to determine whether further
optimization will result in improved performance. In this case, the current
matrix-vector multiplication kernel fully utilizes the GPU DRAM bandwidth for the
large matrices of interest, so further performance improvement has to come from
other transformations. With 85\% of the BiCGSTAB time consumed by the
matrix-vector multiplication kernels, further speedup of 1.17 is the maximum
possible without an algorithm change.
}

\section*{Appendix A: \texttt{scalar-solver} Repository Readme}
\includepdf[pages={1,2}]{README.pdf}


\end{document}
